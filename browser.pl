#!/usr/bin/env perl

use strict;
use utf8;
use open ':encoding(utf8)';
binmode(STDOUT, ":utf8");

use WWW::Mechanize;
use Data::Dumper;
use Gtk2 -init;
use Glib qw/TRUE FALSE/;
use List::Util qw(shuffle);

sub html {
	# http://stackoverflow.com/questions/254345/how-can-i-extract-url-and-link-text-from-html-in-perl
	# http://www.perlmonks.org/?node_id=695886
	my $website = WWW::Mechanize -> new(agent => "NotBlocked/0.01");
	$website -> get($_[0]);
	my @href = $website -> links();
	my %href_hash;
	foreach (@href) {
		my $absolute_path  = $_ -> url_abs() -> abs;
		my $key = sprintf "%s%s", $absolute_path, $_ -> text();
		$href_hash{$key} = [$absolute_path, $_ -> text()];
	}
	return %href_hash;
}

sub x11 {
	$_[0]++;
	my %href_hash = %{$_[1]};
	my @href_keys = @{$_[2]};
	my $window = Gtk2::Window->new;
	if ($href_hash{$href_keys[$_[0]-1]}->[1]) {
		$window->set_title($href_hash{$href_keys[$_[0]-1]}->[1]);
	} else {
		$window->set_title($href_hash{$href_keys[$_[0]-1]}->[0]);
	};
	$window->set_default_size(500, 100);
	$window->signal_connect(destroy => sub { Gtk2->main_quit; });
	my $button = Gtk2::Button->new($href_hash{$href_keys[$_[0]-1]}->[0]);
	$window->add($button);
	$window->show_all;
	my $x = int(rand(1171));
	my $y = int(rand(891));
	$window->move($x, $y);
	if ($_[0]-1 eq $#href_keys) {
		return FALSE;
	} else {
		return TRUE;
	}
}

my %href_hash = &html($ARGV[0]);
my @href_keys = shuffle keys %href_hash;
my $count = 0;
Glib::Timeout->add(200, sub{&x11($count, \%href_hash, \@href_keys)});
Gtk2->main;
